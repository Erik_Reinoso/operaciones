
import cv2
from matplotlib import pyplot as plt
from skimage import io

img1 = io.imread('d.jpg',0)
img2 = io.imread('dm.jpg',0)
resultado2 = cv2.absdiff(img1,img2)

print('img1[0,0]= ', img1[0,0])
print('img2[0,0]= ',img2[0,0])
print('resultado2[0,0]= ',resultado2[0,0])
#cv2.imshow('resultado2',resultado2)

plt.subplot(221), plt.imshow(img1), plt.title('imagen 1')
plt.xticks([]), plt.yticks([])
plt.subplot(222), plt.imshow(img2), plt.title('imagen 2')
plt.xticks([]), plt.yticks([])
plt.subplot(223), plt.imshow(resultado2), plt.title('Resultado')
plt.xticks([]), plt.yticks([])
plt.show()
cv2.waitKey(0)
cv2.destroyAllWindows()