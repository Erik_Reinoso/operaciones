import cv2
import numpy as np
from matplotlib import pyplot as plt

rectangulo= np.zeros((300,300),dtype='uint8')
cv2.rectangle(rectangulo,(25,25),(275,275),255,-1)
circulo=np.zeros((300,300),dtype='uint8')
cv2.circle(circulo,(150,150),150,255,-1)

opand=cv2.bitwise_and(rectangulo,circulo)
opor=cv2.bitwise_or(rectangulo,circulo)
opxor=cv2.bitwise_xor(rectangulo,circulo)


# print('rectangulo[0,0]= ', rectangulo[0,0])
# print('img2[0,0]= ',circulo[0,0])
# print('and[0,0]= ',opand[0,0])
# print('or[0,0]= ',opor[0,0])
# print('xor[0,0]= ',opxor[0,0])

plt.subplot(231),plt.imshow(rectangulo), plt.title('rectangulo')
plt.xticks([]), plt.yticks([])
plt.subplot(232),plt.imshow(circulo), plt.title('circulo')
plt.xticks([]), plt.yticks([])
plt.subplot(233),plt.imshow(opand), plt.title('and')
plt.xticks([]), plt.yticks([])
plt.subplot(234),plt.imshow(opor), plt.title('or')
plt.xticks([]), plt.yticks([])
plt.subplot(235),plt.imshow(opxor), plt.title('xor')
plt.xticks([]), plt.yticks([])
plt.show()

cv2.imshow('imagen',opand)
cv2.waitKey()