import numpy as np
import cv2
from matplotlib import pyplot as plt
from skimage import io

def main():
    img1 = io.imread(r'./d.JPG')
    img2 = io.imread(r'./dm.jpg')

    diffImg1 = cv2.subtract(img1, img2)
    diffImg2 = cv2.subtract(img2, img1)
    diffImg3 = img1 - img2
    diffImg4 = img2 - img1

    print('img1 =  ',img1[0, 0])
    print('img2 =  ', img2[0, 0])
    print('subtract(img1,img2)= ',diffImg1[0, 0])
    print('subtract(img2,img1)= ', diffImg2[0, 0])
    print('img1 - img2 = ', diffImg3[0, 0])
    print('img2 - img1= ', diffImg4[0, 0])
   # print('resultado2[0,0]= ', resultado2[0, 0])

    plt.subplot(231), plt.imshow(img1), plt.title('Original')
    plt.xticks([]), plt.yticks([])
    plt.subplot(232), plt.imshow(img2), plt.title('Modificada')
    plt.xticks([]), plt.yticks([])
    plt.subplot(233), plt.imshow(diffImg1), plt.title('subtract(img1,img2)')
    plt.xticks([]), plt.yticks([])
    plt.subplot(234), plt.imshow(diffImg2), plt.title('subtract(img2,img1)')
    plt.xticks([]), plt.yticks([])
    plt.subplot(235), plt.imshow(diffImg3), plt.title('img1 - img2')
    plt.xticks([]), plt.yticks([])
    plt.subplot(236), plt.imshow(diffImg4), plt.title('img2 - img1')
    plt.xticks([]), plt.yticks([])
    plt.show()

cv2.waitKey(0)
cv2.destroyAllWindows()

main()