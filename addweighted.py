import cv2
from matplotlib import pyplot as plt
from skimage import io

img1 = io.imread('desierto.jpg')


img_res=cv2.resize(img1,dsize=(420,280),interpolation=cv2.INTER_CUBIC)


# Leer imagen 2
img2 = io.imread('universo.jpg')

# Realizar operaciones solicitadas
print(img1.shape)
print(img_res.shape)
print(img2.shape)

resAW = cv2.addWeighted(img_res,0.5,img2,0.9,0)

print('img1 =  ', img1[0, 0])
print('img2 =  ', img2[0, 0])
print('img_res= ', img_res[0, 0])
print('addweighted= ', resAW[0, 0])

plt.subplot(221),plt.imshow(img1), plt.title('primera')
plt.xticks([]), plt.yticks([])
plt.subplot(222),plt.imshow(img2), plt.title('segunda')
plt.xticks([]), plt.yticks([])
plt.subplot(223),plt.imshow(img_res), plt.title('redimensionada')
plt.xticks([]), plt.yticks([])
plt.subplot(224),plt.imshow(resAW), plt.title('fusión')
plt.xticks([]), plt.yticks([])
plt.show()

# cv2.imshow('resAW',resAW)
cv2.waitKey(0)
cv2.destroyAllWindows()