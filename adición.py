import cv2 as cv
from matplotlib import pyplot as plt
from skimage import io

# Usar OpenCV para Adición de imágenes
# cv.add
# Usar OpenCV para Mezcla de imágenes
# cv.addWeughted
# Usar OpenCV para Sustracción  de imágenes
# cv.substract
# cv.absdiff

# Leer imagen Simón Bolívar
img=io.imread('simon_bolivar.jpg')
img_res=cv.resize(img,dsize=(512,512),interpolation=cv.INTER_CUBIC)


# Leer imagen Torre Eiffel
img2=io.imread('torre_eiffel.jpg')

# Realizar operaciones solicitadas
print(img.shape)
print(img_res.shape)
print(img2.shape)
img_add=cv.add(img_res,img2)

print('img1 =  ', img[0, 0])
print('img2 =  ', img2[0, 0])
print('subtract(img1,img2)= ', img_res[0, 0])
print('subtract(img2,img1)= ', img_add[0, 0])

plt.subplot(221),plt.imshow(img), plt.title('primera')
plt.xticks([]), plt.yticks([])
plt.subplot(222),plt.imshow(img2), plt.title('segunda')
plt.xticks([]), plt.yticks([])
plt.subplot(223),plt.imshow(img_res), plt.title('redimensionada')
plt.xticks([]), plt.yticks([])
plt.subplot(224),plt.imshow(img_add), plt.title('fusión')
plt.xticks([]), plt.yticks([])
plt.show()

cv.waitKey()